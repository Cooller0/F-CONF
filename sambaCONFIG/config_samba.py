# config samba
# permission sudo or root
from os import system as limpa

grupo = str(input("Nome do grupo de trabalho: "))
limpa("clear")
#
name_server = str(input("Nome do seu servidor: "))
limpa("clear")
#
print("# share = para não existe autenticação")
print("# user = para exigir autenticação")
acesso = str(input("Modo de acesso aos arquivos linux: "))
limpa("clear")
#
comente = str(input("Comentario do compartilhamento: "))
limpa("clear")
#
local_pasta = str(input("Caminho da pasta que você quer compartilhar: "))
limpa("clear")
#
print("yes or no")
local_divulga = str(input("Se todos os compartilhamentos poderão ser acessados por todos os usuários: "))
limpa("clear")
#
print("yes or no")
local_visao = str(input("Se o compartilhamento será visível ou oculto na rede: "))
limpa("clear")
#
print("yes or no")
local_e = str(input("Se permitirá escrita: "))
limpa("clear")
#
print("yes or no")
local_s_l = str(input("Somente leitura? "))
limpa("clear")
print("""
Primeiro processso OK...
Será criador um arquivo -> /etc/samba/smb.conf
No final do processo reinicie o samba -> sudo /etc/init.d/samba restart ou systemctl restart samba (manjaro)
## HELP ##
# Cria um usuario no linux
sudo adduser mario

# Criar um usuario samba
sudo smbpasswd -a mario
## / HELP ##
""")
config = f"""# PYTHON CONFIG SAMBA
[global]

workgroup = {grupo}
netbios name = {name_server}

## Autenticação ##
security = {acesso}

## ##
[arquivo]

comment = {comente}
path = {local_pasta}
public = {local_divulga}
browseable = {local_visao}
writable = {local_e}
read only = {local_s_l}
create mask = 0700
directory mask = 0700
"""

open("/etc/samba/smb.conf", "w").write(config)
